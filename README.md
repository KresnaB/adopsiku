How to naming branch?

Use grouping tokens (words) at the beginning of your branch names. Use these:
feat   Feature you are adding or expanding
bug    Bug fix

Use / to separate parts of your branch names
Use - to separate words after /


Example: 
feat/register
bug/register
